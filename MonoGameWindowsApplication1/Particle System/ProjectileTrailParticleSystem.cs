#region File Description
//-----------------------------------------------------------------------------
// ProjectileTrailParticleSystem.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace Particle3DSample
{
    /// <summary>
    /// Custom particle system for leaving smoke trails behind the rocket projectiles.
    /// </summary>
    public class ProjectileTrailParticleSystem : ParticleSystem
    {
        public ProjectileTrailParticleSystem(Game game, ContentManager content)
            : base(game, content)
        { }


        protected override void InitializeSettings(ParticleSettings settings)
        {
            settings.TextureName = "textures//Particle002";

            settings.MaxParticles = 22000;

            settings.Duration = TimeSpan.FromSeconds(1.00);

            settings.DurationRandomness = 0.20f;

            settings.EmitterVelocitySensitivity = 0.06f;

            settings.MinHorizontalVelocity = 0;
            settings.MaxHorizontalVelocity = 0;

            settings.MinVerticalVelocity = -0;
            settings.MaxVerticalVelocity = 0;

            settings.MinColor = Color.White;
            settings.MaxColor = Color.White;

            settings.MinRotateSpeed = 0.75f;
            settings.MaxRotateSpeed = 0;

            settings.MinStartSize = 24 * 1.5f;
            settings.MaxStartSize = 24 * 1.5f;

            settings.MinEndSize = 36 * 1.5f; ;
            settings.MaxEndSize = 36 * 1.5f; ;
        }
    }
}
