#region File Description
//-----------------------------------------------------------------------------
// ProjectileTrailParticleSystem.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace Particle3DSample
{
    /// <summary>
    /// Custom particle system for leaving smoke trails behind the rocket projectiles.
    /// </summary>
    public class ShockwaveParticleSystem : ParticleSystem
    {
        public ShockwaveParticleSystem(Game game, ContentManager content)
            : base(game, content)
        { }


        protected override void InitializeSettings(ParticleSettings settings)
        {
            settings.TextureName = "textures//shockwave";

            settings.MaxParticles = 200;

            settings.Duration = TimeSpan.FromSeconds(1.50);

            settings.DurationRandomness = 0.6f;

            settings.EmitterVelocitySensitivity = 0.5f;

            settings.MinHorizontalVelocity = 0.78f;
            settings.MaxHorizontalVelocity = 0.25f;

            settings.MinVerticalVelocity = 0;
            settings.MaxVerticalVelocity = 0;

            settings.MinColor = Color.White;
            settings.MaxColor = Color.White;

            settings.MinRotateSpeed = 0.67f;
            settings.MaxRotateSpeed = 0.25f;

            settings.MinStartSize = 30;
            settings.MaxStartSize = 30;

            settings.MinEndSize = 32;
            settings.MaxEndSize = 32;
            settings.BlendState = BlendState.AlphaBlend;
        }
    }
}
