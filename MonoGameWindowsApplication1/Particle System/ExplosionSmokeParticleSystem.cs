#region File Description
//-----------------------------------------------------------------------------
// ExplosionSmokeParticleSystem.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
#endregion

namespace Particle3DSample
{
    /// <summary>
    /// Custom particle system for creating the smokey part of the explosions.
    /// </summary>
    class ExplosionSmokeParticleSystem : ParticleSystem
    {
        public ExplosionSmokeParticleSystem(Game game, ContentManager content)
            : base(game, content)
        { }


        protected override void InitializeSettings(ParticleSettings settings)
        {
            settings.TextureName = "Textures//smoke";

            settings.MaxParticles = 100;

            settings.Duration = TimeSpan.FromSeconds(1.0);

            settings.MinHorizontalVelocity = 0;
            settings.MaxHorizontalVelocity = 20;

            settings.MinVerticalVelocity = -10;
            settings.MaxVerticalVelocity = 50;

            settings.Gravity = new Vector3(10, -10, 10);

            settings.EndVelocity = 0;

            settings.MinColor = Color.White;
            settings.MaxColor = Color.Transparent;

            settings.MinRotateSpeed = -1;
            settings.MaxRotateSpeed = 3;

            settings.MinStartSize = 100;
            settings.MaxStartSize = 140;

            settings.MinEndSize = 260;
            settings.MaxEndSize = 300;
        }
    }
}
