﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

namespace MonoGameWindowsApplication1
{
        public class SerializerClass
        {
            public void loadMetaData(ref List<shipData> shipDefList, ref List<weaponData> weaponDefList)
            {
                ShipDefList _ShipDefList = new ShipDefList();
                XmlReaderSettings xmlSettings = new XmlReaderSettings();
                XmlReader xmlReader = XmlReader.Create("Resources/shipdefs.xml");
                XmlSerializer serializer = new XmlSerializer(typeof(ShipDefList));
                _ShipDefList = (ShipDefList)serializer.Deserialize(xmlReader);
                foreach (shipData tship in _ShipDefList.ship)
                    shipDefList.Add(tship);
                //xmlReader = XmlReader.Create("Resources/weapondefs.xml");
                //weaponDefList = IntermediateSerializer.Deserialize<List<weaponData>>(xmlReader, null);
               // xmlReader = XmlReader.Create("Resources/listofnames.xml");
               // rNameList = IntermediateSerializer.Deserialize<RandomNames>(xmlReader, null);       
                
             }

            public void exportSaveScenario(List<newShipStruct> activeShipList, string saveName)
            {
                List<saveObject> saveList = new List<saveObject>();
                // Create the data to save
                saveObject saveMe;                
                shipData exportShipDefs = new shipData();                
                XmlWriterSettings xmlSettings = new XmlWriterSettings();
                xmlSettings.Indent = true;

                foreach (newShipStruct ship in activeShipList)
                {
                    saveMe = new saveObject();
                    saveMe.shipPosition = ship.modelPosition;
                    saveMe.shipDirection = ship.targetPosition;
                    saveMe.shipName = ship.objectAlias;
                    saveMe.shipIndex = ship.objectIndex;
                    //saveMe.side = ship.team;
                    saveList.Add(saveMe);
                }

                using (XmlWriter xmlWriter = XmlWriter.Create("Content/XML/Scenarios/" + saveName + ".xml", xmlSettings))
                {
                    //IntermediateSerializer.Serialize(xmlWriter, saveList, null);
                }
            }

            public SceneSaveStruct loadScene(string filename, ref List<newShipStruct> ShipList, ref List<shipData> shipDefList)
            {
                ShipList.Clear();
                //filename = "main_scene.xml";
                    ShipList.Clear();
                    SceneSaveStruct newScene = new SceneSaveStruct();
                    XmlReaderSettings xmlSettings = new XmlReaderSettings();
                    XmlReader xmlReader = XmlReader.Create("Resources/Scenarios/" + filename);
                    //newScene = IntermediateSerializer.Deserialize<SceneSaveStruct>(xmlReader, null);
                    //foreach (saveObject ship in newScene.initalObjectList)
                    //    ShipList.Add(ObjectLoaderClass.loadNewShip(ship.shipPosition, ship.shipIndex, ship.shipName, ref shipDefList, ship.shipDirection, ship.Waypoint));                    
                    //cameraStart = newScene.startingPosition;
                    return newScene;
            }            
    }        
}
