﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MonoGameWindowsApplication1
{
    class MessageClass
    {
        StringBuilder messageBuffer = new StringBuilder();
        public static List<String> messageLog = new List<string>();
        int maxLines = 20;

        public void displaySystemMsg(SpriteFont spriteFont, SpriteBatch spriteBatch, Vector2 systemMessagePos)                         
        {
            spriteBatch.Begin();
            messageBuffer = new StringBuilder();
            foreach (string msg in messageLog)
                messageBuffer.AppendFormat("\n" + msg);
            spriteBatch.DrawString(spriteFont, messageBuffer.ToString(), systemMessagePos, Color.LightGreen);
            //systemMessagePos.Y += 10;
            spriteBatch.End();
            if (messageLog.Count > maxLines)
                messageLog.Remove(messageLog[0]);
        }
    }
}
