  using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using Particle3DSample;

namespace MonoGameWindowsApplication1
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class WeaponsManager : ModelManager
    {
        Random rand = new Random();

        public List<weaponStruct> activeWeaponList = new List<weaponStruct>();
        List<Projectile> projectiles = new List<Projectile>();
        // Laser Setup info
        EffectParameter effect_center_to_viewer;
        EffectParameter effect_color;
        EffectParameter effect_matrices_combined;
        EffectTechnique effect_technique;
        Matrix[] shader_matrices_combined = new Matrix[2];
        Effect laserEffect;
        Vector3 turretDirection = Vector3.Zero;
        float thrustAmount;
        double elapsedTime;
        double currentTime;
        ParticleSystem projectileTrailParticles;
        public ParticleEmitter trailEmitter;
        ContentManager content;

       /// <summary>
        /// Velocity scalar to approximate drag.
        /// </summary>
        private const float DragFactor = 0.98f;
        Texture2D hRibbonTexture;
        Texture2D hBeamTexture;
      

        public WeaponsManager(Game game)
            : base(game)
        {
            // TODO: Construct any child components here
        }

        public void Initialize(Game game, ParticleSystem projectileTrailParticlesNew)
        {

        }

        public Model LaserModelLoad(string modelFileName)
        {
            myModel = Game.Content.Load<Model>(modelFileName);
            foreach (ModelMesh mesh in myModel.Meshes)
                foreach (ModelMeshPart part in mesh.MeshParts)
                    part.Effect = laserEffect;
            return myModel;
        }
      
        public void updateMissileMovement(GameTime gameTime, float gameSpeed, weaponStruct thisObject, Camera camera)
        {
            currentTime = gameTime.TotalGameTime.TotalMilliseconds;
            float turningSpeed = (float)gameTime.ElapsedGameTime.TotalMilliseconds / 1000.0f;
            float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;

            thrustAmount = 1.00f;
            turningSpeed = thisObject.objectAgility;

            if (thisObject.isHoming)
            {
                thisObject.targetPosition = Vector3.Normalize(thisObject.missileTarget.modelPosition - thisObject.modelPosition);
                thisObject.modelRotation.Forward = Vector3.SmoothStep(thisObject.Direction, thisObject.targetPosition, turningSpeed);
            }
            else if (thisObject.objectClass == WeaponClassEnum.Beam)
            {
                thisObject.targetPosition = thisObject.Direction;
                thisObject.modelRotation.Forward = thisObject.targetPosition;
            }
            else if (!thisObject.isHoming)
            {
                thisObject.targetPosition = thisObject.modelRotation.Forward;
                thisObject.modelRotation.Forward = thisObject.targetPosition;
            }

            Vector3 scale, translation;
            Quaternion rotation;
            Matrix rotationMatrix = Matrix.CreateWorld(thisObject.modelPosition, thisObject.targetPosition, thisObject.Up);
            rotationMatrix.Decompose(out scale, out rotation, out translation);
            thisObject.Up = Vector3.TransformNormal(thisObject.Up, rotationMatrix);
            thisObject.Up.Normalize();
            thisObject.right = Vector3.Cross(thisObject.targetPosition, thisObject.Up);
            thisObject.Up = Vector3.Cross(thisObject.right, thisObject.modelRotation.Forward);
            thisObject.Direction = thisObject.modelRotation.Forward;
            Vector3 force = thisObject.Direction * thrustAmount * thisObject.objectThrust;
            // Apply acceleration
            Vector3 acceleration = force / thisObject.objectMass;
            thisObject.Velocity *= DragFactor;
            thisObject.Velocity += acceleration * elapsed;// *gameSpeed;
            //}
            if (thisObject.objectClass != WeaponClassEnum.Beam && !thisObject.isCollision)
                thisObject.modelPosition += thisObject.Velocity * elapsed;
            thisObject.worldMatrix = Matrix.CreateWorld(thisObject.modelPosition, thisObject.Direction, Vector3.Up);
            thisObject.distanceFromOrigin = Vector3.Distance(thisObject.modelPosition, thisObject.missileOrigin.modelPosition);
            thisObject.modelBoundingSphere.Center = thisObject.modelPosition;            
           if (thisObject.projectile != null) thisObject.projectile.Update(gameTime, thisObject.modelPosition);          
           thisObject.timer += 10; // currentTime - elapsedTime;            
            thisObject.screenCords = get2dCoords(thisObject.modelPosition, camera);
            elapsedTime = currentTime;            
        }
      
        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public void Update(GameTime gameTime, float gameSpeed,  Camera camera)
        {
            // TODO: Add your update code here
            for (int i=0; i < activeWeaponList.Count; i++)
            {
                updateMissileMovement(gameTime, gameSpeed, activeWeaponList[i], camera);
                if (activeWeaponList[i].objectClass == WeaponClassEnum.Beam)
                {
                    if (activeWeaponList[i].timeToLive < activeWeaponList[i].timer)
                        activeWeaponList.Remove(activeWeaponList[i]);
                }
                else
                {
                    if ((activeWeaponList[i].isCollision) || activeWeaponList[i].timeToLive < activeWeaponList[i].timer
                        || activeWeaponList[i].distanceFromOrigin > activeWeaponList[i].range || (activeWeaponList[i].missileTarget != null && activeWeaponList[i].missileTarget.hullLvl < 1)
                        || (activeWeaponList[i].isHoming && activeWeaponList[i].missileTarget == null))
                    {                      
                        activeWeaponList.Remove(activeWeaponList[i]);
                    }
                }
            }
            base.Update(gameTime);
        }
     
//////////////////////// NEW FIRE WEAPON CLASS
        public void fireWeapon(newShipStruct targetObject, newShipStruct weaponOrigin, Vector3 direction, float rotateAmount, float offset,
            int weaponIndex, List<weaponData> weaponDefList, Vector4 mposition)
        {
            if (Game1.isEdit) return;
            weaponStruct tempData;
            tempData = new weaponStruct();
            tempData.targetPosition = Vector3.Zero;
            Vector3 force = Vector3.Zero;            
            float distance = 0.0f; ;
            if (targetObject != null)
            {
                distance = Vector3.Distance(targetObject.modelPosition, weaponOrigin.modelPosition);
                tempData.targetPosition = targetObject.modelPosition - weaponOrigin.modelPosition;                
                tempData.Direction = Vector3.Normalize(targetObject.modelPosition - tempData.modelPosition);
            }
            //else
            //    tempData.targetPosition = (weaponOrigin.modelPosition + weaponOrigin.Direction * 1000) - weaponOrigin.modelPosition;
            tempData.objectFileName = "Models//LaserBolt2";
            tempData.radius = 25;
            tempData.isProjectile =  weaponDefList[weaponIndex].isProjectile;
            tempData.isHoming = weaponDefList[weaponIndex].isHoming;
            tempData.objectClass = weaponDefList[weaponIndex].wClass;
            tempData.objectColor = Color.LightBlue; // weaponDefList[0].weaponColor;
            tempData.objectScale = 340;
            tempData.damageFactor = weaponDefList[weaponIndex].damageFactor;            
            tempData.missileTarget = targetObject;
            tempData.objectAgility = weaponDefList[weaponIndex].Agility;
            tempData.objectMass = weaponDefList[weaponIndex].Mass;
            tempData.objectThrust = weaponDefList[weaponIndex].Thrust;
            tempData.length = weaponDefList[weaponIndex].length;
            tempData.radius = weaponDefList[weaponIndex].radius;
            tempData.texture = weaponDefList[weaponIndex].texture;
            tempData.modelBoundingSphere = new BoundingSphere(tempData.modelPosition, tempData.radius);
            tempData.missileOrigin = weaponOrigin;
           // tempData.Velocity = weaponOrigin.Velocity;
            tempData.modelPosition = weaponOrigin.modelPosition + new Vector3(mposition.X, 0, mposition.Z) * rand.Next(1,2);        
            tempData.modelRotation = weaponOrigin.modelRotation;             
            Color color = Color.White;
            Color color2 = Color.White;           
            if (tempData.isProjectile)
            {
                Matrix rotationToBeDone = Matrix.Identity;
                rotationToBeDone = Matrix.CreateRotationY(rotateAmount);
                Vector3 FinalVector;
                FinalVector = Vector3.Transform(direction, rotationToBeDone);
                force = FinalVector * 1 * (tempData.objectThrust / 10);                
            }
            tempData.initalVelocity = tempData.Direction;
            tempData.Velocity += force;
            tempData.modelPosition = weaponOrigin.modelPosition;
            //tempData.modelPosition += weaponOrigin.Up;
           // if (!tempData.isProjectile)
            tempData.modelPosition += weaponOrigin.modelRotation.Down * 10.25f;
            tempData.modelPosition += weaponOrigin.modelRotation.Forward * 10.25f;
            tempData.modelPosition += direction * offset;
            tempData.modelRotation = weaponOrigin.modelRotation;

            if (tempData.objectClass == WeaponClassEnum.Beam)
            {
                color = Color.Green;
                color2 = Color.Green;
                tempData.radius = 50;
                //tempData.trail = TrailFactory.GetNewTrail(Game, tempData.length, tempData.radius, true, color2, Color.Transparent, BlendState.AlphaBlend, true, TrailTexturing.BEAM, hBeamTexture, 1, SamplerState.LinearClamp);
                //tempData.trail.Reset(tempData.modelPosition);
                //Game.Components.Add(tempData.trail);
            }
            else
            {
                if (tempData.objectClass == WeaponClassEnum.Cannon) color = Color.Green;
                if (tempData.objectClass == WeaponClassEnum.Torpedo) color = Color.Red;
                if (tempData.objectClass == WeaponClassEnum.Missile) color = Color.Blue;
                if (tempData.objectClass == WeaponClassEnum.Missile || tempData.objectClass == WeaponClassEnum.Torpedo)
                {
                    tempData.projectile = new Projectile(tempData.missileOrigin.localParticleSystem, tempData.modelPosition, tempData.Velocity);                    
                    //tempData.trail = TrailFactory.GetNewTrail(Game, tempData.length * 2, tempData.radius *2, true, Color.White, color, BlendState.AlphaBlend, true);
                    //tempData.trail = TrailFactory.GetNewTrail(Game, tempData.length * 4, tempData.radius * 3f, true, Color.White, color, BlendState.AlphaBlend, true, TrailTexturing.STRETCHED, hRibbonTexture, 1, SamplerState.LinearClamp);
                    //tempData.trail.Reset(tempData.modelPosition);
                    //Game.Components.Add(tempData.trail);
                }
            }

            tempData.timeToLive = weaponDefList[weaponIndex].timeToLive;
            tempData.timer = 0;
            tempData.regenTime = weaponDefList[weaponIndex].regenTime;
            tempData.range = weaponDefList[weaponIndex].range;
            tempData.worldMatrix = Matrix.CreateWorld(tempData.modelPosition, tempData.targetPosition, Vector3.Up);           
            activeWeaponList.Add(tempData);
        }


        public static void set_mesh(ModelMesh mesh, GraphicsDevice device)
        {
            device.SetVertexBuffer(mesh.MeshParts[0].VertexBuffer);
            device.SetVertexBuffer(mesh.MeshParts[0].VertexBuffer);
            device.Indices = mesh.MeshParts[0].IndexBuffer;
        }

        public static void draw_set_mesh(ModelMesh mesh, GraphicsDevice device)
        {

            device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0,
                                         mesh.MeshParts[0].NumVertices, mesh.MeshParts[0].StartIndex,
                                         mesh.MeshParts[0].PrimitiveCount);
        }

        public void DrawLaser(GraphicsDevice device, Matrix view, Matrix projection, Color laserColor, weaponStruct weapon, Model model)
        {
            laserEffect.CurrentTechnique = effect_technique;
            if (activeWeaponList.Count > 0)
            {
                //Matrix wMatrix = Matrix.CreateScale(new Vector3(20, 1, weapon.objectScale* 50)) * weapon.worldMatrix;
                Matrix wMatrix = Matrix.CreateScale(weapon.radius * 4, 2, weapon.length * 6) * weapon.worldMatrix;
                //set the mesh on the GPU
                set_mesh(model.Meshes[0], device);
                laserEffect.CurrentTechnique.Passes[0].Apply();
                shader_matrices_combined[0] = wMatrix;
                shader_matrices_combined[1] = wMatrix * view * projection;
                effect_matrices_combined.SetValue(shader_matrices_combined);
                effect_color.SetValue(laserColor.ToVector4());
                effect_center_to_viewer.SetValue(Vector3.Up);
                //laserEffect.CommitChanges();
               draw_set_mesh(model.Meshes[0], device);
            }
        }

    }
}