﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace MonoGameWindowsApplication1
{
    public class Listbox
    {
        public List<ListBoxItem> items;
        public Color color, hoverColor, backgroundColor;
        public Vector2 position;
        public int itemCount,spacing;
        public bool inBox;
        Texture2D dummy;

        public void InitListBox(Color color, Color hoverColor, Color backgroundColor, Vector2 position, Texture2D dummyTexture)
        {
            items = new List<ListBoxItem>();
            this.color = color;
            this.hoverColor = hoverColor;
            this.position = position;
            itemCount = 0;
            spacing = 20;
            dummy = dummyTexture;
        }

        public void AddItem(string text)
        {
            ListBoxItem newitem = new ListBoxItem();
            newitem.text = text;
            newitem.rectangle = new Rectangle((int)position.X, (int)position.Y + (itemCount * spacing), 150, 15);
            itemCount++;
            items.Add(newitem);
        }

        public int Selected()
        {
            int i = 0;
            foreach (ListBoxItem lb in items)
            {
                if (lb.isSelected)
                    return (i);
                i++;
            }
            return -1;
        }

        public void Update(MouseState currentMouseState)
        {
            foreach (ListBoxItem lb in items)
            {
                if (lb.rectangle.Intersects(new Rectangle((int)currentMouseState.X, (int)currentMouseState.Y, 1, 1)) && currentMouseState.LeftButton == ButtonState.Pressed)
                {
                    foreach (ListBoxItem lb2 in items) lb2.isSelected = false;
                    lb.isSelected = true;                
                }               
            }
        }

        public void Draw(SpriteBatch spriteBatch, SpriteFont spriteFont)
        {
            Color thisColor;
            Vector2 tempPosition = position;
            foreach (ListBoxItem lbi in items)
            {
                thisColor = color;
                if (lbi.isSelected) thisColor = hoverColor;
                spriteBatch.Draw(dummy, lbi.rectangle, Color.Blue);
                spriteBatch.DrawString(spriteFont, lbi.text, tempPosition, thisColor);
                tempPosition.Y += spacing;
            }

        }
    }
}
