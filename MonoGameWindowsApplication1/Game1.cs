﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Storage;
//using Particle3DSample;
#endregion

namespace MonoGameWindowsApplication1
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Game
    {
        Random rand = new Random();
        float gameSpeed = 1.0f;
        double currentTime;
        int screenX;
        int screenY;
        Listbox shipListBox, sideListBox;
        public static bool isEdit = false;
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont regularFont;
        SpriteFont largeFont;
        SpriteFont medFont;
        Camera camera;
        ModelManager modelManager;
        NPCManager npcManager;
        WeaponsManager weaponsManager;
        InputClass inputClass;
        MouseClass mouseClass;
        SerializerClass serializerClass;
        VertexDeclaration quadVertexDecl;
        DrawQuadClass drawQuad;
        BasicEffect quadEffect;
        SkySphere skySphere;

        //ParticleSystem explosionParticles;
       // ParticleSystem explosionSmokeParticles;
       // ParticleSystem projectileTrailParticles;
       // ParticleSystem smokePlumeParticles;
       // Projectile projectile;

        MouseState oldMouseState;

        public static Dictionary<string, Model> modelDictionary = new Dictionary<string, Model>();
        public Dictionary<string, Texture2D> textureDictionary = new Dictionary<string, Texture2D>();
        public KeyboardState currentKeyboardState;
        public List<shipData> shipDefList = new List<shipData>();
        public List<weaponData> weaponDefList = new List<weaponData>();
        public static List<newShipStruct> objectList = new List<newShipStruct>();
        /// <summary>
        /// Declare Assets        
        /// </summary>
        Texture2D explosion, explosion2, smoke;


        public Game1()
            : base()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            ConfigureGraphicsManager();
            serializerClass = new SerializerClass();
            inputClass = new InputClass();
            mouseClass = new MouseClass();
            mouseClass.Init(this);
            camera = new Camera(this);
            camera.SetUpCamera();
            modelManager = new ModelManager(this);
            modelManager.Initialize();
            npcManager = new NPCManager(this);
            weaponsManager = new WeaponsManager(this);
            //weaponsManager.Initialize(this, null);
            drawQuad = new DrawQuadClass();
            skySphere = new SkySphere(this);
            //guiClass = new NFClass();
            //skySphere.LoadSkySphere(this);  
           // explosionParticles = new ParticleSystem(this, Content, "ExplosionSettings");
            //explosionParticles.Initialize();
           // Components.Add(explosionParticles);
            base.Initialize();
        }

        public void ConfigureGraphicsManager()
        {
            screenX = 1920;
            screenY = 1080;
            graphics.PreferredBackBufferWidth = screenX;
            graphics.PreferredBackBufferHeight = screenY;
            graphics.IsFullScreen = false;
            this.IsMouseVisible = true;
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            regularFont = Content.Load<SpriteFont>("Fonts/SmallFont");
            largeFont = Content.Load<SpriteFont>("Fonts/LargeFont");
            medFont = Content.Load<SpriteFont>("Fonts/MedFont");            
            //modelDictionary.Add("noble_frigate", Content.Load<Model>("Models/Noble Frigate/b_class_frigate"));
            //modelDictionary.Add("ares_fighter", Content.Load<Model>("Models/Ares Fighter/ares_fighter"));
            quadEffect = new BasicEffect(graphics.GraphicsDevice);
            quadVertexDecl = new VertexDeclaration(VertexPositionNormalTexture.VertexDeclaration.GetVertexElements());
            loadMetaData();
            objectList.Add(ObjectLoaderClass.loadNewShip(this, Vector3.Zero, 0, "Hello World", ref shipDefList, Vector3.Left, Vector3.Zero, 0));
            objectList.Add(ObjectLoaderClass.loadNewShip(this, HelperClass.RandomPosition(0,1000), 0, "Hello World", ref shipDefList, Vector3.Right, Vector3.Zero, 0));
            // TODO: use this.Content to load your game content here
            explosion = Content.Load<Texture2D>("Textures/Particles/explosion2");
            explosion2 = Content.Load<Texture2D>("Textures/Particles/explosion3");
            smoke = Content.Load<Texture2D>("Textures/Particles/smoke");
        }

        private void loadMetaData()
        {
            /// <summary>
            /// Load Ship/Unit Data from XML            
            /// </summary>
            /// <param name="ShipData">Provide Reference to List to populate</param>
            serializerClass.loadMetaData(ref shipDefList, ref weaponDefList);
            
            sideListBox = new Listbox();
            sideListBox.InitListBox(Color.White, Color.Gray, Color.DarkBlue, new Vector2(100, 100), Content.Load<Texture2D>("Textures/Beam_green"));
            sideListBox.AddItem("USN");
            sideListBox.AddItem("Eriax");
            shipListBox = new Listbox();
            shipListBox.InitListBox(Color.White, Color.Gray, Color.DarkBlue, new Vector2(100, 10), Content.Load<Texture2D>("Textures/Beam3"));
            foreach (shipData thisShip in shipDefList)
            {
                if (!modelDictionary.ContainsKey(thisShip.FileName))
                    modelDictionary.Add(thisShip.FileName, Content.Load<Model>(thisShip.FileName));
                shipListBox.AddItem(thisShip.Type);
            }
            foreach (weaponData thisWeapon in weaponDefList)
                if (!modelDictionary.ContainsKey(thisWeapon.FileName))
                    modelDictionary.Add(thisWeapon.FileName, weaponsManager.LaserModelLoad(thisWeapon.FileName));
            textureDictionary.Add("beam_blue", Content.Load<Texture2D>("Textures/Beam3"));
            textureDictionary.Add("beam_green", Content.Load<Texture2D>("Textures/beam_green"));
            textureDictionary.Add("beam_white", Content.Load<Texture2D>("Textures/beam_white"));
            textureDictionary.Add("circle_red", Content.Load<Texture2D>("Textures/circle_red"));           
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
            inputClass.handleInput(gameTime, objectList, camera);
            mouseClass.Update(camera, GraphicsDevice.Viewport, ref objectList);
            camera.UpdateChaseCamera(Vector3.Zero, isEdit);
            foreach (newShipStruct tship in objectList)
            {
                npcManager.performAI(gameTime, ref weaponsManager, ref weaponDefList, tship);
                npcManager.updateShipMovement(gameTime, gameSpeed, tship, camera);
            }
            // TODO: Add your update logic here

            if (isEdit)
            {
                shipListBox.Update(Mouse.GetState());
                sideListBox.Update(Mouse.GetState());
                if (Mouse.GetState().RightButton == ButtonState.Pressed && oldMouseState.RightButton == ButtonState.Released)
                {
                    newShipStruct newShip = ObjectLoaderClass.loadNewShip(this, mouseClass.mouse3dVector(camera, GraphicsDevice.Viewport), shipListBox.Selected(), 
                                            "My Fighter", ref shipDefList, Vector3.Forward, Vector3.Zero, sideListBox.Selected());
                    npcManager.updateShipMovement(gameTime, gameSpeed, newShip, camera);
                    objectList.Add(newShip);
                }
            }
            oldMouseState = Mouse.GetState();
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            // TODO: Add your drawing code here
            //skySphere.DrawSkySphere(this, camera);
            mouseClass.Draw(spriteBatch);           
            GraphicsDevice.BlendState = BlendState.Opaque;
            GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            shipDraw(gameTime);
            spriteBatch.Begin();
            if (isEdit)
            {
                Vector2 offset = largeFont.MeasureString("Edit Mode");
                spriteBatch.DrawString(largeFont, "EDIT MODE", new Vector2(screenX / 2 - offset.X, 25), Color.White);
                shipListBox.Draw(spriteBatch, regularFont);
                sideListBox.Draw(spriteBatch, regularFont);
            }
            Color textColor = Color.White;
            foreach (newShipStruct ship in objectList)
            {
                if (ship.team == 1) textColor = Color.Orange;
                spriteBatch.DrawString(regularFont, ship.objectAlias.ToString(), new Vector2(ship.screenCords.X, ship.screenCords.Y - 20), textColor);
                textColor = Color.White;
            }

            spriteBatch.End();
            //modelManager.DrawModel(camera, modelDictionary["noble_frigate"], Matrix.Identity, Color.White);
            base.Draw(gameTime);
        }

        private void shipDraw(GameTime gameTime)
        {
            foreach (newShipStruct thisShip in objectList)
            {
                modelManager.DrawModel(camera, modelDictionary[thisShip.objectFileName], thisShip.worldMatrix, Color.White);
                if (thisShip.isSelected)
                    drawQuad.DrawQuad(GraphicsDevice, quadVertexDecl, quadEffect, camera.viewMatrix, camera.projectionMatrix,
                    new Quad(Vector3.Zero, -camera.cameraRotation.Forward, Vector3.Up, thisShip.modelLen * 3.25f, thisShip.modelLen * 3.25f, true), 
                    textureDictionary["circle_red"], Vector3.Forward, thisShip.modelPosition);
                //debug(thisShip);
            }
        }
    }
}
