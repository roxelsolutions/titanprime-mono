using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

namespace MonoGameWindowsApplication1
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class NPCManager : ModelManager
    {       
        Random rand = new Random();
        Vector3 isFacing;
        Vector3 isRight;
        int moduleCount;
        double currentTime;

        /// <summary>
        /// Velocity scalar to approximate drag.
        /// </summary>
        private const float DragFactor = 0.97f;

        public NPCManager(Game game)
            : base(game)
        {
        }

        public void updateShipMovement(GameTime gameTime, float gameSpeed, newShipStruct thisShip, Camera ourCamera)
        {
            thisShip.vecToTarget = Vector3.Normalize(thisShip.targetPosition - thisShip.modelPosition);
            thisShip.thrustAmount = 1.0f;            
            float turningSpeed = (float)gameTime.ElapsedGameTime.TotalMilliseconds / 1000.0f;
            float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;
            turningSpeed = thisShip.objectAgility * gameSpeed;
            thisShip.Direction = Vector3.Normalize(Vector3.Lerp(thisShip.Direction, thisShip.vecToTarget,
                                 turningSpeed * 0.045f));
            thisShip.modelRotation.Forward = thisShip.Direction;
            thisShip.modelRotation.Right = Vector3.Cross(thisShip.Direction, Vector3.Up);
            thisShip.modelRotation.Up = Vector3.Up;
            Vector3 force = thisShip.Direction * thisShip.thrustAmount * thisShip.objectThrust;
            // Apply acceleration
            Vector3 acceleration = force / thisShip.objectMass;
            thisShip.Velocity *= DragFactor;
            thisShip.Velocity += acceleration * elapsed * gameSpeed;
            thisShip.modelPosition += thisShip.Velocity * elapsed;
            thisShip.worldMatrix = Matrix.CreateWorld(thisShip.modelPosition, thisShip.Direction, thisShip.Up);
            thisShip.modelBoundingSphere.Center = thisShip.modelPosition;
            thisShip.viewMatrix = Matrix.CreateLookAt(thisShip.modelPosition, thisShip.modelPosition +
                                                      thisShip.Direction * 2.0f, thisShip.Up);
            Matrix cMatrix = Matrix.CreateLookAt(thisShip.modelPosition, thisShip.modelPosition + thisShip.modelRotation.Right * thisShip.modelWidth, thisShip.Up);           
           
            thisShip.screenCords = get2dCoords(thisShip.modelPosition, ourCamera);

                Vector3 bsCenter = thisShip.modelPosition;
                bsCenter = thisShip.modelPosition + (thisShip.Direction * thisShip.modelLen / 3);
                thisShip.bsList[0] = new BoundingSphere(bsCenter, thisShip.modelLen / 6);
                bsCenter = thisShip.modelPosition + (-thisShip.Direction * thisShip.modelLen / 3);
                thisShip.bsList[1] = new BoundingSphere(bsCenter, thisShip.modelLen / 6);            
                thisShip.bsList[2] = new BoundingSphere(thisShip.modelPosition, thisShip.modelLen / 6);

                bsCenter = thisShip.modelPosition + (thisShip.modelRotation.Right * thisShip.modelWidth / 3);
                thisShip.bsList[3] = new BoundingSphere(bsCenter, thisShip.modelWidth / 4);
                bsCenter = thisShip.modelPosition + (-thisShip.modelRotation.Right * thisShip.modelWidth / 3);
                thisShip.bsList[4] = new BoundingSphere(bsCenter, thisShip.modelWidth / 4);               

        }

        public void performAI(GameTime gameTime, ref WeaponsManager weaponsManager, ref List<weaponData> weaponDefList, newShipStruct thisShip)
        {
            //isSquad = false;
            currentTime = gameTime.TotalGameTime.TotalMilliseconds;
            if (thisShip.currentDisposition == disposition.moving)
            {
                thisShip.targetPosition = thisShip.wayPointPosition;
                if (thisShip.modelBoundingSphere.Intersects(new BoundingSphere(thisShip.wayPointPosition, 50)))
                    thisShip.currentDisposition = disposition.patrol;
            }
            else
                thisShip.targetPosition = (thisShip.modelPosition + thisShip.Direction) * 100;           
            if (Vector3.Distance(thisShip.modelPosition, Vector3.Zero) > 65000)
                thisShip.targetPosition = Vector3.Zero;
        }

        public void cycleWeapons(newShipStruct thisShip, newShipStruct otherShip, double currentTime, WeaponsManager weaponsManager,
          List<weaponData> weaponDefList)
        {
            thisShip.angleOfAttack = MathHelper.ToDegrees((float)GetSignedAngleBetweenTwoVectors(thisShip.modelRotation.Forward, thisShip.vecToTarget, otherShip.modelRotation.Right));
            foreach (WeaponModule thisWeapon in thisShip.weaponArray)
            {
                for (int i = 0; i < thisWeapon.ModulePositionOnShip.Count(); i++)
                {
                switch ((int)thisWeapon.ModulePositionOnShip[i].W)
                {
                    case 0:
                        isFacing = thisShip.modelRotation.Forward;
                        isRight = thisShip.modelRotation.Right;
                        break;
                    case 1:
                        isFacing = -thisShip.modelRotation.Forward;
                        isRight = -thisShip.modelRotation.Right;
                        break;
                    case 2:
                        isFacing = -thisShip.modelRotation.Right;
                        isRight = thisShip.modelRotation.Forward;
                        break;
                    case 3:
                        isFacing = thisShip.modelRotation.Right;
                        isRight = -thisShip.modelRotation.Forward;
                        break;
                }
                if ( Vector3.Distance(thisShip.modelPosition, thisShip.targetPosition) < weaponDefList[(int)thisWeapon.weaponType].range 
                        && currentTime - thisShip.regenTimer[moduleCount] > weaponDefList[(int)thisWeapon.weaponType].regenTime)
                        {
                                    if (thisWeapon.weaponType == WeaponTypeEnum.Rocket_Pack)
                                    {
                                        weaponsManager.fireWeapon(thisShip.currentTarget, thisShip, thisShip.right, 15, 5, (int)thisWeapon.weaponType, weaponDefList, thisWeapon.ModulePositionOnShip[i]);
                                        weaponsManager.fireWeapon(thisShip.currentTarget, thisShip, thisShip.right, 15, 5, (int)thisWeapon.weaponType, weaponDefList, thisWeapon.ModulePositionOnShip[i]);
                                    }
                                    else
                                        weaponsManager.fireWeapon(thisShip.currentTarget, thisShip, thisShip.right, 15, 5, (int)thisWeapon.weaponType, weaponDefList, thisWeapon.ModulePositionOnShip[i]);
                                    thisShip.regenTimer[moduleCount] = currentTime;
                                    thisShip.isEngaging = true;
                                    //return;
                             }
                    moduleCount++;
                }
            //i++;
             }
        }

        /// Find the angle between two vectors. This will not only give the angle difference, but the direction.
        /// For example, it may give you -1 radian, or 1 radian, depending on the direction. Angle given will be the 
        /// angle from the FromVector to the DestVector, in radians.
        /// </summary>
        /// <param name="FromVector">Vector to start at.</param>
        /// <param name="DestVector">Destination vector.</param>
        /// <param name="DestVectorsRight">Right vector of the destination vector</param>
        /// <returns>Signed angle, in radians</returns>        
        /// <remarks>All three vectors must lie along the same plane.</remarks>

        public static double GetSignedAngleBetweenTwoVectors(Vector3 Source, Vector3 Dest, Vector3 DestsRight)
        {
            // We make sure all of our vectors are unit length
            Source.Normalize();
            Dest.Normalize();
            DestsRight.Normalize();

            float forwardDot = Vector3.Dot(Source, Dest);
            float rightDot = Vector3.Dot(Source, DestsRight);

            // Make sure we stay in range no matter what, so Acos doesn't fail later
            forwardDot = MathHelper.Clamp(forwardDot, -1.0f, 1.0f);

            double angleBetween = Math.Acos((float)forwardDot);

            if (rightDot < 0.0f)
                angleBetween *= -1.0f;

            return angleBetween;
        }
    }
}                        