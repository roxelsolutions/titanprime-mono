﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace MonoGameWindowsApplication1
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class Camera : Microsoft.Xna.Framework.GameComponent
    {
        //Camera variables
        Vector3 cameraReference = new Vector3(0, 0, 1);
        public Vector3 campos = new Vector3(95, 1800.0f, 1.0f);
        //public Vector3 edit_campos = new Vector3(1, 500, 1);
        public static Vector3 edit_campos = new Vector3(95, 1800.0f, 1.0f);
        public Vector3 lootAtPosition = Vector3.Zero;
        public Matrix cameraRotation = Matrix.Identity;
        public Matrix viewMatrix;
        public Matrix projectionMatrix;
        public static float cameraUp = 50.0f;
        public static Vector3 camup;
        public static Vector3 lookAt;
        public static bool cameraisup;
        public static float zoomFactor = 4;
        public static float maxZoom = 20;

        public float AspectRatio
        {
            get { return aspectRatio; }
            set { aspectRatio = value; }
        }
        private float aspectRatio = 4.0f / 3.0f;

        public Camera(Game game)
            : base(game)
        {
            // TODO: Construct any child components here
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            // TODO: Add your initialization code here

            base.Initialize();
        }

        public void SetUpCamera()
        {
            //viewMatrix = Matrix.CreateLookAt(new Vector3(1,250,800), Vector3.Zero, Vector3.Up);
            projectionMatrix = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver2, Game.GraphicsDevice.Viewport.AspectRatio, 0.5f, 60000.0f);
        }

        public void UpdateChaseCamera(Vector3 position, bool isEdit)
        {
           
            campos = Vector3.Lerp(campos, lootAtPosition + edit_campos * zoomFactor, 0.15f);
            campos = Vector3.Transform(campos, cameraRotation);
            camup = Vector3.Up;
            camup = Vector3.Transform(camup, cameraRotation);
            lookAt = Vector3.Transform(lootAtPosition, Matrix.Identity);
            lookAt.Y = 10;
            viewMatrix = Matrix.CreateLookAt(campos, lookAt, camup);           
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            // TODO: Add your update code here
            base.Update(gameTime);
        }
    }
}

