﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;


namespace MonoGameWindowsApplication1
{
    public class DrawQuadClass
    {
        
        public void DrawQuad(GraphicsDevice graphicsDevice, VertexDeclaration quadVertexDecl, BasicEffect quadEffect, Matrix View, Matrix Projection, Quad quad, Texture2D texture,Vector3 forward, Vector3 playerPos)
        {
            graphicsDevice.BlendState = BlendState.AlphaBlend;
            graphicsDevice.DepthStencilState = DepthStencilState.Default;
            //Game1.graphics.GraphicsDevice.RenderState.DepthBufferEnable = true;
            //quadEffect.EnableDefaultLighting();            
            quadEffect.World = Matrix.CreateRotationX(MathHelper.ToRadians(-90)) * Matrix.CreateWorld(playerPos, forward, Vector3.Up);
            quadEffect.View = View;
            quadEffect.Projection = Projection;
            quadEffect.TextureEnabled = true;
            quadEffect.Texture = texture;

            //graphicsDevice.VertexDeclaration = quadVertexDecl;
            //quadEffect.Begin();
            foreach (EffectPass pass in quadEffect.CurrentTechnique.Passes)
            {
                pass.Apply();
                graphicsDevice.DrawUserIndexedPrimitives<VertexPositionNormalTexture>(
                    PrimitiveType.TriangleList, quad.Vertices, 0, 4, quad.Indexes, 0, 2);
            }

           // graphicsDevice.RenderState.AlphaBlendEnable = false;
        }
      
}
}