﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using TomShane.Neoforce.Controls;
using EventArgs = TomShane.Neoforce.Controls.EventArgs;

namespace MonoGameWindowsApplication1
{
    class NFClass
    {
        public Window commandPanel, detailPanel;
        // Create our buttons.
        Button saveBtn, cancelBtn, clearBtn;
        ListBox ShipListBox;
        RadioButton sideRB, sideRB2;
        Label title;
        Renderer render;
        //SliderBar capitalshipSlider, frigateSlider, fighterSlider;

        List<newShipStruct> activeShipList = new List<newShipStruct>();
        public TomShane.Neoforce.Controls.Console consoleLogWindow;
        Texture2D blueCircle, greenCircle;
        private List<object> items = new List<object>();
        MouseClass mouseClass;
        MouseState currentMouse, prevMouse;
        int objectIndex, IFF;
        Game game;

        public void LoadAssets(Game game, MouseClass mclass)
        {
            blueCircle = game.Content.Load<Texture2D>("Textures/Editor/blueTranscircle");
            greenCircle = game.Content.Load<Texture2D>("Textures/Editor/transplanegreen");
            mouseClass = mclass;
            this.game = game;
        }

        public void EditModeUpdate(ref List<newShipStruct> shipList, ref List<shipData> shipData, Camera camera, Viewport vp)
        {
            currentMouse = Mouse.GetState();
            if (currentMouse.RightButton == ButtonState.Pressed && prevMouse.RightButton == ButtonState.Released)
            {
                if (sideRB.Checked) IFF = 0;
                if (sideRB2.Checked) IFF = 1;
                //shipList.Add(ObjectLoaderClass.loadNewShip(game, mouseClass.mouse3dVector(camera, vp), objectIndex, "New Ship", ref shipData, Vector3.Zero, Vector3.Zero, IFF, ClassesEnum.Fighter));
                string mystring = "Adding Ship: " + mouseClass.mouse3dVector(camera, vp).ToString();
                //consoleLogWindow.MessageBuffer.Add(new ConsoleMessage(mystring, 1));
                System.Console.Error.WriteLine(mystring);
            }

            foreach (newShipStruct t in shipList)
            {
                if (t.isSelected)
                {
                    //detailPanel.Visible = true;
                    // break;
                }
               // detailPanel.Visible = false;
            }
            prevMouse = currentMouse;
        }

        public void Draw(SpriteBatch spriteBatch, ref List<newShipStruct> shipList)
        {
            spriteBatch.Begin();
            foreach (newShipStruct t in shipList)
            {
                if (t.isSelected)
                {
                    spriteBatch.Draw(blueCircle, new Rectangle((int)t.screenCords.X-32, (int)t.screenCords.Y-32, 64, 64), Color.Yellow);
                }
            }
            spriteBatch.End();
        }

        public void LoadCommandWindow(Game game, Manager manager, ref List<shipData> shipList)
        {
            /// Define ship Command Panel
            /// 
            commandPanel = new Window(manager);
            commandPanel.Text = "Mission Editor";
            commandPanel.Visible = true;
            commandPanel.Resizable = false;
            commandPanel.IconVisible = true;
            commandPanel.CloseButtonVisible = false;
            commandPanel.Color = Color.Black;
            commandPanel.Top = 50;
            commandPanel.Left = 850;
            commandPanel.Width = 300;
            commandPanel.Height = 800;
            commandPanel.Alpha = 192;
            /// Command Panel Title
            /// 
            title = new Label(manager);
            title.Text = "Available Objects:";
            title.Top = 80;
            title.Left = 10;
            title.Width = 150;
            title.Color = Color.White;
            title.Parent = commandPanel;
            /// Object Listbox define
            /// 
            ShipListBox = new ListBox(manager);
            ShipListBox.Top = 100;
            ShipListBox.Left = 10;
            ShipListBox.Width = 200;
            ShipListBox.Height = 150;
            ShipListBox.Text = "Available Ships";
            ShipListBox.Init();
            ShipListBox.Parent = commandPanel;
            string[] data = new string[shipList.Count()];
            int i = 0;
            foreach (shipData t in shipList)
            {
                    data[i] = t.Type;
                    i++;
            }
            ShipListBox.Items.AddRange(data);
            ShipListBox.ItemIndexChanged += new TomShane.Neoforce.Controls.EventHandler(_cboClasses_ItemIndexChanged);
            /// Save Button
            /// 
            saveBtn = new Button(manager);
            saveBtn.Top = 25;
            saveBtn.Left = 135;
            saveBtn.Width = 100;
            saveBtn.Text = "Save";
            saveBtn.Parent = commandPanel;   
            /// Clear scene button
            /// 
            clearBtn = new Button(manager);
            clearBtn.Top = 25;
            clearBtn.Left = 25;
            clearBtn.Width = 75;
            clearBtn.Text = "Clear All";
            clearBtn.Parent = commandPanel;

            title = new Label(manager);
            title.Text = "IFF:";
            title.Top = 260;
            title.Left = 10;
            title.Width = 150;
            title.Color = Color.White;
            title.Parent = commandPanel;
            sideRB = new RadioButton(manager);
            sideRB.Init();
            sideRB.Click += SortClick;
            sideRB.Text = "Federated";
            sideRB.Left = 20;
            sideRB.Top = 280;
            sideRB.Width = 150;
            sideRB.Checked = true;
            sideRB.Parent = commandPanel;
            sideRB2 = new RadioButton(manager);
            sideRB2.Init();
            sideRB2.Click += SortClick;
            sideRB2.Text = "Eriax";
            sideRB2.Left = 20;
            sideRB2.Top = 300;
            sideRB2.Parent = commandPanel;
            sideRB2.Width = 150;

            title = new Label(manager);
            title.Text = "Selected Ship Options:";
            title.Top = 320;
            title.Left = 10;
            title.Width = 150;
            title.Color = Color.White;
            title.Parent = commandPanel;

            title = new Label(manager);
            title.Text = "Target Preferences:";
            title.Top = 340;
            title.Left = 20;
            title.Width = 150;
            title.Color = Color.White;
            title.Parent = commandPanel;

            title = new Label(manager);
            title.Text = "Capitalship";
            title.Top = 360;
            title.Left = 30;
            title.Width = 150;
            title.Color = Color.Gray;
            title.Parent = commandPanel;

            title = new Label(manager);
            title.Text = "Frigate";
            title.Top = 380;
            title.Left = 30;
            title.Width = 150;
            title.Color = Color.Gray;
            title.Parent = commandPanel;

            title = new Label(manager);
            title.Text = "Fighter";
            title.Top = 400;
            title.Left = 30;
            title.Width = 150;
            title.Color = Color.Gray;
            title.Parent = commandPanel;

         
           // fighterSlider.Parent = commandPanel;

            ListBox spawnTimer = new ListBox(manager);
            spawnTimer.Init();
            spawnTimer.Parent = detailPanel;
            spawnTimer.Top = 10;
            spawnTimer.Left = 50;
            string[] times = new string[6];
            times[0] = "0 - No Timer";
            times[1] = "1";
            times[2] = "2";
            times[3] = "3";
            times[4] = "4";
            times[5] = "5";
            spawnTimer.Visible = false;            
            ///Hook Button to Events
            ///
            saveBtn.Click += saveBtn_Click;
            clearBtn.Click += clearBtn_Click;

            manager.Add(commandPanel);           
        }

        #region Button Handlers

        void _cboClasses_ItemIndexChanged(object sender, TomShane.Neoforce.Controls.EventArgs e)
        {
            if (ShipListBox.ItemIndex > -1)
                objectIndex = ShipListBox.ItemIndex;
        }

        void _cboClasses_IFFIndexChanged(object sender, TomShane.Neoforce.Controls.EventArgs e)
        {
            IFF = 0;
        }

        void SortClick(object sender, EventArgs e)
        {
        
        }

        void saveBtn_Click(object sender, EventArgs e)
        {
            
        }

        void clearBtn_Click(object sender, EventArgs e)
        {
            Game1.objectList.Clear();
        }
        #endregion

        public void LoadConsoleWindow(Manager manager)
        {
            consoleLogWindow = new TomShane.Neoforce.Controls.Console(manager);
            consoleLogWindow.Init();
            consoleLogWindow.Visible = true;
            consoleLogWindow.Top = 800;
            consoleLogWindow.Left = 10;
            consoleLogWindow.Width = 600;
            consoleLogWindow.Alpha = 128;
            manager.Add(consoleLogWindow);
        }      
    }
}

