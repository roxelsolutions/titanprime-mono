﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Controls;
using TrailDLL;

namespace MonoGameWindowsApplication1
{
    public class ListBoxItem
    {
        public string text;
        public Rectangle rectangle;
        public bool isHover;
        public bool isSelected;
        public int index;
    }

    public enum WeaponClassEnum
    {
        Cannon = 0,
        Missile = 1,
        Torpedo = 2,
        Beam = 3
    }

    public enum WeaponTypeEnum
    {
        PointDefense = 0,
        AutoCannon = 1,
        SR_Missile = 2,
        Rocket_Pack = 3,
        LRH_Missile = 4,
        Ion = 5,
        Mass_Driver = 6
    }

    public enum Orders
    {
        patrol,
        orbit,
        stop,
        hunt,
        defend
    }

    public enum disposition
    {
        engaging = 0,
        patrol = 1,
        idle = 2,
        moving = 3,
        defensive = 4,
        nofire = 5,
        mining = 6,
        building = 7
    }

    public class newShipStruct
    {
        public int objectIndex;
        public string objectFileName;
        public string objectAlias;
        public float objectMass;
        public float objectThrust;
        public float objectScale;
        public string objectType;
        public float objectAgility;
        public float hullFactor;
        public float hullLvl;
        public float shieldFactor;
        public float shieldLvl;
        public float hullValue;
        public float shieldValue;
        public float radius;
        public float modelLen;
        public float modelWidth;
        public double angleOfAttack;
        public int techLevel;
        public int team;
        public ClassesEnum objectClass;
        public ClassesEnum preferredClass;
        public Matrix worldMatrix;
        public Vector3 modelPosition;
        public Vector3 editModeOffset;
        public Vector3 targetPosition;
        public Vector3 wayPointPosition;
        public Vector3 vecToTarget;
        public Vector3 initalVelocity;
        public Vector3 Velocity;
        public Vector3 Direction;
        public Vector3 Up;
        public Vector3 right;
        public Vector3 Right;
        public Vector3 ThrusterPosition;
        public Vector3 screenCords; 
        public Matrix modelRotation;
        public disposition currentDisposition;
        public BoundingSphere modelBoundingSphere;
        public BoundingSphere[] bsList;
        public newShipStruct currentTarget;    
        public Model shipModel;
        public float thrustAmount;
        public float distanceFromTarget;       
        public Matrix viewMatrix;
        public Matrix projectionMatrix;
        public bool ThrusterEngaged = false;
        public bool isEvading;
        public bool isEngaging;
        public bool isCollision;
        public bool isSelected;
        public double[] regenTimer;
        public double timer;
        public float currentTargetLevel;
        public int currentTargetIndex;
        public float[] EvadeDist;
        public float[] TargetPrefs;
        public int[] ChasePrefs;
        public WeaponModule[] weaponArray;
        public int currentWeaponIndex;
        public Orders orders;
    }

    public class weaponStruct : newShipStruct
    {
        public bool isProjectile;
        public bool isHoming;
        public int regenTime;
        public int damageFactor;
        public newShipStruct missileOrigin;
        public float distanceFromOrigin;
        public float range;
        public Color objectColor;
         public newShipStruct missileTarget;
        public double timeToLive;
        public WeaponClassEnum objectClass;
        public int modIndex;
        public int length;
        public float radius;
        public String color;
        public bool isSwarm;
        public int swarmSize;
        public string texture;
    }

    [Serializable]
    public class genericObjectLoadClass
    {
        public string FileName;
        public string Type;
        public int TechLevel;
        public float Mass;
        public float Thrust;
        public float SphereRadius;
        public float Scale;
        public float Agility;
        public string BelongsTo;
    }

    public class shipData : genericObjectLoadClass
    {
        public ClassesEnum Class;
        public float ShieldFactor;
        public float HullFactor;
        public float hullValue;
        public float shieldValue;
        public int ShieldRegenTime;
        public float[] EvadeDist;
        public float[] TargetPrefs;
        public int[] ChasePrefs;
        public float maxDetectRange;
        public bool isStationary;
        public Vector3 ThrusterPosition;
        public int numFighterWings;
        //public WeaponModule[] AvailableWeapons;
    }  

    public class weaponData : genericObjectLoadClass
    {
        public WeaponClassEnum wClass;
        public int length;
        public int radius;
        public String color;
        public bool isProjectile;
        public bool isHoming;
        public bool isSwarm;
        public int swarmSize;
        public string texture;
        public int regenTime;
        public int damageFactor;
        public float range;
        public int timeToLive;
    }

    public class WeaponModule
    {
        public WeaponTypeEnum weaponType;
        public Vector4[] ModulePositionOnShip;
        public float FiringEnvelopeAngle;
        public float weaponRange;
        public double timer;
    }

    public enum ClassesEnum
    {
        Fighter = 0,
        Crusier = 1,
        Carrier = 2,
        Frigate = 3,
        Bomber = 4,
        SWACS = 5,
        Freighter = 6,
        Platform = 7,
        None = 8
    }

        public enum DirectionEnum
        {
            front = 0, // Bow
            rear = 1, // Stern
            left = 2, // Port
            right = 3 // Starboard
        }

        public class planetStruct
        {
            public Vector3 planetPosition;
            public Texture2D planetTexture;
            public int planetRadius;
            public Model planetModel;
            public BoundingSphere planetBS;
            public int isControlled;
            public Vector3 screenCoords;
            public string planetName;
            public bool isSelected;
            public int aResourceAmount;
        }

        public class planetSaveStruct
        {
            public int planetTextureFile;
            public int planetRadius;
            public Vector3 planetPosition;
            public int isControlled;
            public string planetName;
            public int aResourceAmount;
        }

        public enum TextBoxActions
        {
            SaveScenario,
            LoadScenario,
            SaveMap,
            LoadMap,
            None
        }

        public enum MouseActions
        {
            None,
            AddUnit,
            MoveUnit,
            SelectUnit,
            PaintHex
        }

        public enum GameModes
        {
            None,
            EditMode,
            UnitCommandMode,
            CombatMode,
            MoveMode
        }

        [Serializable]
        public struct saveObject
        {
            public int shipIndex;
            public string shipName;
            public Vector3 shipPosition;
            public Vector3 shipDirection;
            public int side;
            public Vector3 Waypoint;
        }

        public class SceneSaveStruct
        {
            public MissonConstruct mission;
        }

        public class systemStruct
        {
            public SceneSaveStruct systemScene;
            public List<newShipStruct> systemShipList;
            //public PlanetManager pManager;
            public Vector3 lastCameraPos;
            public WeaponsManager weaponsManager;
        }

        public class ShipDefList
        {
            public List<shipData> ship;
        }

        [Serializable]
        public class RandomNames
        {
            public List<string> capitalShipNames;
        }
}
