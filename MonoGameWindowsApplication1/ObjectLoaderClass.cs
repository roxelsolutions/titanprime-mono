﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MonoGameWindowsApplication1
{
    class ObjectLoaderClass
    {
        public static newShipStruct loadNewShip(Game game, Vector3 position, int objectIndex, string name, ref List<shipData> Shipdata, Vector3 direction, Vector3 wayPoint, int IFF)
        {
            newShipStruct newShip;
            newShip = new newShipStruct();
            newShip.objectMass = 2.0f;
            newShip.Up = Vector3.Up;
            newShip.Right = Vector3.Zero;
            newShip.Direction = direction;
            newShip.Direction.Y = 0;
            newShip.worldMatrix = Matrix.Identity;
            newShip.modelRotation = Matrix.Identity;
            newShip.modelPosition = position;
            newShip.modelPosition.Y = 0;
            newShip.wayPointPosition = wayPoint;
            newShip.objectFileName = Shipdata[objectIndex].FileName;
            newShip.objectIndex = objectIndex;
            newShip.objectAlias = name;
            if (Shipdata[objectIndex].Class == ClassesEnum.Fighter)
                newShip.objectAlias = "Fighter";
            newShip.EvadeDist = Shipdata[objectIndex].EvadeDist;
            newShip.objectThrust = Shipdata[objectIndex].Thrust;
            newShip.objectAgility = Shipdata[objectIndex].Agility;
            newShip.objectClass = Shipdata[objectIndex].Class;
            newShip.TargetPrefs = Shipdata[objectIndex].TargetPrefs;
            newShip.ChasePrefs = Shipdata[objectIndex].ChasePrefs;
            newShip.objectMass = Shipdata[objectIndex].Mass;
            newShip.objectType = Shipdata[objectIndex].Type;            
            newShip.hullLvl = Shipdata[objectIndex].hullValue;
            newShip.bsList = new BoundingSphere[6];
           // newShip.localParticleSystem = new ProjectileTrailParticleSystem(game, game.Content);
           // newShip.localParticleSystem.Initialize();
          //  game.Components.Add(newShip.localParticleSystem);
            newShip.team = IFF;
            //newShip.weaponArray = Shipdata[objectIndex].AvailableWeapons;            
            newShip.modelLen = Shipdata[objectIndex].SphereRadius;
            newShip.modelBoundingSphere = new BoundingSphere(position, newShip.modelLen/2);            
            //Build Bounding Frustrum for all Weapon Modules on ship
            return newShip;
        }      
    }
}
