﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Storage;

namespace MonoGameWindowsApplication1
{
    public class Mission
    {
        Random rand = new Random();
        Game game;
        /// <summary>
        /// Go through Event list to find and load any Events that are scheduled to happen on startup
        /// </summary>
        /// <param name="mission"></param>
        /// <param name="shipList"></param>
        /// <param name="shipDefList"></param>
        public void MissionInitalLoad(Game game, ref SceneSaveStruct mission, ref List<newShipStruct> shipList, ref List<shipData> shipDefList)
        {
            this.game = game;
            foreach (MissionEvent tEvent in mission.mission.mEvents)
            {
                switch (tEvent.eTrigger)
                {
                    /// EventTrigger.None is a null eTrigger that is used to trigger an event on the start of the Mission
                    case EventTrigger.None:                        
                        MissonAddShips(ref shipList, ref shipDefList, mission.mission.mFlightGroups[tEvent.eFlightGroupIndex]);                        
                        break;
                }
            }
        }

        public void MissionCheckUp(ref SceneSaveStruct mission, ref List<newShipStruct> shipList, ref List<shipData> shipDefList, double missionTimer)
        {
            foreach (MissionEvent tEvent in mission.mission.mEvents)
            {
                switch (tEvent.eTrigger)
                {
                    /// EventTrigger.None is a null eTrigger that is used to trigger an event on the start of the Mission
                    case EventTrigger.None:
                        break;
                    case EventTrigger.TimeTrigger:
                        if (missionTimer > tEvent.eDelay && !tEvent.hasOccurred)
                        {
                            MissonAddShips(ref shipList, ref shipDefList, mission.mission.mFlightGroups[tEvent.eFlightGroupIndex]);
                            tEvent.hasOccurred = true;
                        }
                        break;
                }
            }
        }

        private void MissonAddShips(ref List<newShipStruct> shipList, ref List<shipData> shipDefList, FlightGroup fg)
        {
                        for (int i = 0; i < fg.fgCount; i++)
                        {
                            int ii = i + 1;
                            float t = 2.0f;
                            if (fg.fgCount > 1)
                                t = (float)rand.NextDouble();

                            newShipStruct tShip = ObjectLoaderClass.loadNewShip(game, Vector3.Multiply(fg.fgPosition, t), (int)fg.fgClass,
                                fg.fgDesignation + ii, ref shipDefList, Vector3.Zero, Vector3.Zero, fg.fgIFF);
                            tShip.preferredClass = fg.fgGoal1.goalShipClass;
                            tShip.orders = fg.fgOrders;
                            MessageClass.messageLog.Add("Alert: Ship " + fg.fgDesignation + " "+ii + " Detected");
                            shipList.Add(tShip);
                        }
        }
    }
}
