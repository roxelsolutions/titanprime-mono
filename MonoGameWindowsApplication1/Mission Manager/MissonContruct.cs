﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Storage;
//using Microsoft.Xna.Framework.Content.Pipeline.Serialization.Intermediate;

namespace MonoGameWindowsApplication1
{
    public enum EventTrigger
    {
        None = 0,
        TimeTrigger = 1,
        ProximityTrigger = 2,    
    }

    public enum EventAction
    {
        SpawnShip = 0,
        RemoveShip = 1,
    }
   
    public enum FGStatus
    {
        Normal = 0,
        Stranded = 1,
        Damaged = 2,
    }

    /// <summary>
    /// (Number) of (Ship Class) must (Outcome)
    /// (100%) of (Freighters) must (Destroyed)
    /// </summary>
    public class FGGoal
    {
        public int goalNumber;
        public string goalTarget;
        public ClassesEnum goalShipClass;
        public GoalOutcome goalOutcome;
    }

    public enum GoalOutcome
    {
        Survive = 0,
        BeDestroyed = 1,
        BeInspected = 2,
    }

    public class FlightGroup
    {
        public int fgIndex;
        public string fgDesignation;
        public int fgCount;
        public ClassesEnum fgClass;
        public int fgIFF;
        public Orders fgOrders;
        public string fgProtectTarget;
        public Vector3 fgPosition;
        public FGStatus fgStatus;
        public FGGoal fgGoal1;        
    }
    
    public class MissionEvent
    {
        public string eDescription;
        public EventTrigger eTrigger;
        public double eDelay;
        public Vector3 eLocation;
        public EventAction eAction;
        public int eFlightGroupIndex;
        public bool hasOccurred;
    }

    public class MissonConstruct
    {
        public string mName;
        public string mObjectiveText;
        public List<MissionEvent> mEvents;
        public List<FlightGroup> mFlightGroups;
    }
}
