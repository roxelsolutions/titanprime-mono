﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace MonoGameWindowsApplication1
{
    class InputClass
    {
        KeyboardState oldKeyboardState;
        MouseState mouseStatePrevious;
        KeyboardState currentKeyboardState;
        MouseState mouseStateCurrent;
        Random rand = new Random();
        double lastKeyPressTime, currentTime;
        public void handleInput(GameTime gameTime, List<newShipStruct> shipList, Camera camera)
        {
            double typeSpeed = 10;
            currentKeyboardState = Keyboard.GetState();
            mouseStateCurrent = Mouse.GetState();
            double currentTime = gameTime.TotalGameTime.TotalMilliseconds;

            if (mouseStateCurrent.ScrollWheelValue > mouseStatePrevious.ScrollWheelValue)
            {
                float WheelVal = (mouseStateCurrent.ScrollWheelValue -
                             mouseStatePrevious.ScrollWheelValue) / 120;
                if (Camera.zoomFactor < Camera.maxZoom)
                    Camera.zoomFactor += (WheelVal * 1f);
            }
            //! Scroll-Down | Zoom Out
            if (mouseStateCurrent.ScrollWheelValue < mouseStatePrevious.ScrollWheelValue)
            {
                float WheelVal = (mouseStateCurrent.ScrollWheelValue -
                             mouseStatePrevious.ScrollWheelValue) / 120;
                if (Camera.zoomFactor > 1.0)
                    Camera.zoomFactor -= (WheelVal * -1f);
                if (Camera.zoomFactor < 1.0)
                    Camera.zoomFactor = 2.0f;
            }

            if (currentTime - lastKeyPressTime > typeSpeed)
            {
                float scrollSpeed = 35 * Camera.zoomFactor;
                /// Select Closet target
                /// 
                if (currentKeyboardState.IsKeyDown(Keys.Tab) && oldKeyboardState.IsKeyUp(Keys.Tab))
                {
                    Camera.cameraisup = true;
                    //playerShip.currentTarget = shipList[0];

                }

                if (currentKeyboardState.IsKeyDown(Keys.D1))
                {
                    Camera.edit_campos.X -= 10;
                }
                if (currentKeyboardState.IsKeyDown(Keys.D2))
                {
                    Camera.edit_campos.X += 10;
                }
                if (currentKeyboardState.IsKeyDown(Keys.D3))
                {
                   Camera.edit_campos.Z += 10;
                }
                if (currentKeyboardState.IsKeyDown(Keys.D4))
                {
                   Camera.edit_campos.Z -= 10;
                }

                if (currentKeyboardState.IsKeyDown(Keys.S))
                {
                    camera.campos.X += scrollSpeed;
                    camera.lootAtPosition.X += scrollSpeed;
                }
                if (currentKeyboardState.IsKeyDown(Keys.W))
                {
                    camera.campos.X -= scrollSpeed;
                    camera.lootAtPosition.X -= scrollSpeed;
                }
                if (currentKeyboardState.IsKeyDown(Keys.D))
                {
                    camera.campos.Z -= scrollSpeed;
                    camera.lootAtPosition.Z -= scrollSpeed;
                }
                if (currentKeyboardState.IsKeyDown(Keys.A))
                {
                    camera.campos.Z += scrollSpeed;
                    camera.lootAtPosition.Z += scrollSpeed;
                }
                /// Select Closet target
                /// 
                if (currentKeyboardState.IsKeyDown(Keys.E) && oldKeyboardState.IsKeyUp(Keys.E))
                {
                    if (Game1.isEdit)
                        Game1.isEdit = false;
                    else
                        Game1.isEdit = true;
                }
                lastKeyPressTime = currentTime;
            }
            //panCamera();
            oldKeyboardState = currentKeyboardState;
            mouseStatePrevious = mouseStateCurrent;
        }
    }
}
