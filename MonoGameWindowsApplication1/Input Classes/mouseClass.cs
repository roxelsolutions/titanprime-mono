﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace MonoGameWindowsApplication1
{
    class MouseClass
    {
        float disFromcenter;
        public Rectangle selectionRect;
        Texture2D selectRecTex;
        bool isGroupSelect;
        MouseState oldMouseState;
        float scrollSpeed = 15;

        public void Init(Game game)
        {
            selectRecTex = game.Content.Load<Texture2D>("Textures/Editor/SelectionBox");
        }

        public Vector3 mouse3dVector(Camera ourCamera, Viewport vp)
        {
            MouseState mouseState = Mouse.GetState();
            Vector2 ms = new Vector2(mouseState.X, mouseState.Y);
                //vp = GraphicsDevice.Viewport;
                //  Note the order of the parameters! Projection first.
                Vector3 pos1 = vp.Unproject(new Vector3(ms.X, ms.Y, 0), ourCamera.projectionMatrix, ourCamera.viewMatrix, Matrix.Identity);
                Vector3 pos2 = vp.Unproject(new Vector3(ms.X, ms.Y, 1), ourCamera.projectionMatrix, ourCamera.viewMatrix, Matrix.Identity);
                Vector3 dir = Vector3.Normalize(pos2 - pos1);
                Vector3 ppos = Vector3.Zero;
                //  If the mouse ray is aimed parallel with the world plane, then don't 
                //  intersect, because that would divide by zero.
                if (dir.Y != 0)
                {
                    Vector3 x = pos1 - dir * (pos1.Y / dir.Y);
                    ppos = x;
                    disFromcenter = Vector3.Distance(ppos, Vector3.Zero);
                    return ppos;
                }
                return Vector3.Zero;
        }

        public void Update(Camera camera, Viewport vp, ref List<newShipStruct> objectList)
        {            
            if (Mouse.GetState().LeftButton == ButtonState.Pressed && (Keyboard.GetState().IsKeyDown(Keys.LeftShift) || isGroupSelect))
            {
                if (isGroupSelect == false && !Game1.isEdit)
                    unSelectAll(ref objectList);
                isGroupSelect = true;
                selectRectangle(Mouse.GetState(), mouse3dVector(camera, vp));
            }

            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
                unSelectAll(ref objectList);
            //else
            if (Mouse.GetState().LeftButton == ButtonState.Released)
            {
                selectionRect = Rectangle.Empty;
                isGroupSelect = false;
            }
            if (isGroupSelect)
                RectangleSelect(ref objectList, vp, camera.projectionMatrix, camera.viewMatrix, selectionRect);

            if (Mouse.GetState().RightButton == ButtonState.Pressed)
            {
                foreach (newShipStruct ship in objectList)
                {
                    if (ship.isSelected)
                    {
                        ship.currentDisposition = disposition.moving;
                        ship.wayPointPosition = mouse3dVector(camera, vp);
                        ship.orders = Orders.patrol;
                    }
                }
            }

            if (Mouse.GetState().LeftButton == ButtonState.Pressed && oldMouseState.LeftButton == ButtonState.Released)
            {
                foreach (newShipStruct ship in objectList)
                {
                    if (ship.modelBoundingSphere.Intersects(new BoundingSphere(mouse3dVector(camera, vp), 50)))
                    {
                        ship.isSelected = true;
                    }
                    else
                        ship.isSelected = false;
                }
            } else 
                /// Move Camera with mouse
                if (Mouse.GetState().LeftButton == ButtonState.Pressed && (Keyboard.GetState().IsKeyUp(Keys.LeftShift)))
                {
                    Vector2 v1 = new Vector2(Mouse.GetState().X, Mouse.GetState().Y);
                    Vector2 v2 = new Vector2(oldMouseState.X, oldMouseState.Y);
                    float xdiff = Mouse.GetState().X - oldMouseState.X;
                    float ydiff = Mouse.GetState().Y - oldMouseState.Y;
                    //camera.campos.X += -ydiff * scrollSpeed;
                   // camera.lootAtPosition.X += -ydiff * scrollSpeed;
                    //camera.campos.Z += xdiff * scrollSpeed;
                   // camera.lootAtPosition.Z += xdiff * scrollSpeed;

                }
            oldMouseState = Mouse.GetState();
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (selectionRect != Rectangle.Empty)
            {
                spriteBatch.Begin();
                spriteBatch.Draw(selectRecTex, fixRectangle(), Color.White);
                spriteBatch.End();
            }
        }
      
        public Rectangle selectRectangle(MouseState mouseState, Vector3 mouse3d)
        {
            if (selectionRect == Rectangle.Empty)
                selectionRect = new Rectangle((int)mouseState.X, (int)mouseState.Y, 0, 0);
            else if (selectionRect != Rectangle.Empty)
            {
                selectionRect.Width = mouseState.X - selectionRect.X;
                selectionRect.Height = mouseState.Y - selectionRect.Y;
            } //else
            return selectionRect;
        }

        public static void unSelectAll(ref List<newShipStruct> objectList)
        {
            foreach (newShipStruct o in objectList)
                o.isSelected = false;
        }

        public static void RectangleSelect(ref List<newShipStruct> objectsList, Viewport viewport, Matrix projection, Matrix view, Rectangle selectionRect)
        {
            int i = 0;
            foreach (newShipStruct o in objectsList)
            {
                Vector3 screenPos = viewport.Project(o.modelPosition, projection, view, Matrix.Identity);
                if (selectionRect.Contains((int)screenPos.X, (int)screenPos.Y))
                {
                    o.isSelected = true;
                    i++;
                }
            }
        }

        public Rectangle fixRectangle()
        {
            /// Some janky code to prevent the rectangle from flipping or something....
            /// 
            Rectangle r = new Rectangle(selectionRect.X, selectionRect.Y, selectionRect.Width, selectionRect.Height);
            if (r.Width < 0)
            {
                r.Width = -r.Width;
                r.X -= r.Width;
            }
            if (r.Height < 0)
            {
                r.Height = -r.Height;
                r.Y -= r.Height;
            }
            return r;
        }

    }
}
